#!/bin/bash

DJANGO_SETTINGS=config.settings.$APP_ENV
DJANGO_WSGI=config.wsgi
LOG_FILE=/project/logs/gunicorn.log
LOG_LEVEL=debug
NUM_WORKERS=3
NAME=project
HOST=0.0.0.0
PORT=8000

# Delete any existing sock and pid files
rm -f $PIDFILE $SOCKFILE

echo "-> Starting gunicorn"
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS
export PYTHONPATH=/project:\$PYTHONPATH
exec gunicorn \
    ${DJANGO_WSGI} \
    --name $NAME \
    --workers $NUM_WORKERS \
    --bind=$HOST:$PORT \
    --log-level=$LOG_LEVEL \
    --log-file=$LOG_FILE \
    --pid=$PIDFILE \
