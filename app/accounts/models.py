from django.db import models
from django.contrib.auth.models import AbstractUser

from .managers import AccountManager


class Account(AbstractUser):
    username = None
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = AccountManager()
