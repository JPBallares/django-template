from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import Account

PERSONAL_INFO_FIELDS = (
    'Personal info', {
        'fields': (
            'first_name',
            'last_name',
        )
    }
)

PERMISSION_FIELDS = (
    'Permissions', {
        'fields': (
            'is_active',
            'is_staff',
            'is_superuser',
            'groups',
            'user_permissions',
        )
    }
)

IMPORTANT_DATES_FIELDS = (
    'Important dates', {
        'fields': (
            'last_login',
            'date_joined'
        )
    }
)


@admin.register(Account)
class AccountAdmin(UserAdmin):
    """Define admin model for custom User model with no email field."""

    fieldsets = (
        ('Credentials', {
            'fields': (
                'email',
                'password',
            )
        }),
        PERSONAL_INFO_FIELDS,
        PERMISSION_FIELDS,
        IMPORTANT_DATES_FIELDS,
    )

    add_fieldsets = (
        ('Credentials', {
            'fields': (
                'email',
                'password1',
                'password2',
            )
        }),
        PERSONAL_INFO_FIELDS,
        PERMISSION_FIELDS,
    )

    list_display = (
        'email',
        'first_name',
        'last_name',
        'is_active',
        'last_login',
    )

    search_fields = (
        'email',
        'first_name',
        'last_name',
    )

    list_filter = (
        'is_active',
    )

    ordering = ('-id',)
