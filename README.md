# Django Project template

## First thing to do

Find and replace all `project` occurrences in the following file
- `config/settings/base.py`
- `config/settings/prod.py`
- `config/settings/dev.py`
- `scripts/gunicorn/start.sh`
- `scripts/nginx/nginx.conf`
- `docker-compose.yml`

Setting up your compose file
- Create your own copy of the dev compose file and edit the web port mapping.
- Update docker-compose.yml to fit your needs
- Create also you own copy of django settings file. You'll place here any configuration needed only for development. This prevents accidentally commiting changes to the base settings file

```
$ cp docker-compose.yml dev_yourname.yml
$ cp config/settings/dev.py config/settings/dev_yourname.py
```

Rename app.env.sample to app.env, then update the app.env and provide the following
| Variable | Description |
|----------------|---------------------------------|
| SECRET_KEY | Secret key settings of Django |
| APP_ENV | Name of the Django settings file you've created (e.g `APP_ENV=dev` will use `config/settings/dev.py`). Typically "dev_yourname" |
| POSTGRES_USER | Username of Postgres default User to be created                             |
| POSTGRES_DB | Name of default Postgres Database to be created |
| POSTGRES_PASSWORD | Password for the default Postgres User |

Great, all files are now set.


## Running docker-compose
Remember the file you copied from docker-compose, you will use that to start your own containers
```
$ docker-compose -f dev_yourname.yml up -d
```

You can connect to container by using the following command
```
$ docker exec -it dev-project_name-web bash
# python3 --version
```

This is useful running migrations and migrate command from django.
