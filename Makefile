SHELL := /bin/bash

assets:
	python3 manage.py collectstatic --no-input -v 0

shell:
	python3 manage.py shell

migrations:
	python3 manage.py makemigrations

migrate:
	python3 manage.py migrate

jsreverse:
	python3 manage.py collectstatic_js_reverse

superuser:
	python3 manage.py createsuperuser

startapp:
	python3 manage.py startapp $(APP)
